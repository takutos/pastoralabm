#!/usr/bin/python
"""
This file contains code for running Nomads simulation model: an agent-based model simulating pastoralist spatial movements.

Copyright 2015 Takuto Sakamoto
License: GNU GPLv3 http://www.gnu.org/licenses/gpl.html
"""

import sys
import os
import csv
import math
from datetime import datetime

import matplotlib
matplotlib.use('TkAgg')

import pylab as PL
import random as RD
import scipy as SP
import pymas

RD.seed()

monthsInYear = 12		# length of a year
yearsInGeneration = 10		# length of a generation
generationsInRun = 5000		# length of a simulation run
repetition = 1		# number of repetition of simulation runs
input1 = 'LCLU'		# name of input data file 1 (land cover and land use)
input2 = 'ClimateZones'		# name of input data file 2 (climate zones)
droughtSeverity = [0.5, 1.0, 0.5]	# effect of different types of drought event ([moderate drought, major drought, major drought on riverine areas])
tsetseSeverity = [0.25, 0.5, 1.0]	# effect of different types of tsetse event ([minor tsetse, moderate tsetse, massive tsetse])

population = 1		# population of Nomad agents
moveRange = 5		# distance that can be moved without incurring cost
grazeRange = 0		# range of neighborhood to be grazed
scoutRange = 5		# range of neighborhood to be scouted for information
scoutFrequency = 0.1	# scouting frequency in one month
numOfAlts = 100		# maximum number of alternative routes to be considered in the adaptation phase
exploreTendency = 0.1	# tendency to allow major changes in the movement plan
movementCost = 10	# cost incurred when moving beyond a specified distance in one month
adaptParams = [-12.0, 100, 0.00001, 0.01]		# adaptParams: parameters controlling the agent's update of the movement plan ([minimum potential normalized to 0.0, reference potential normalized to 1.0, adaptation noise for minimum potential, daptation noise for reference potential])
inheritance = True		# an option for activating knowledge inheritance between generations

drawingMode = 0		# landscape information to be shown (0: LCLU, 1: ClimateZones, 2: Resources, 3: Drought, 4: Tsetse)
showRoute = True	# an option for showing the movement paths of agents
outInterval = 120		# time interval for displaying the output

settingOut = False
processOut = False
snapshot = False
succesiveShots = False
shotInterval = 100

#set control variables
conVars = [generationsInRun, population, outInterval]
conSetting = [[generationsInRun, 'scale', 'int', 'Generations', 1, 10000, 1],
			[population, 'scale', 'int', 'Population', 1, 100, 1],
			[outInterval, 'scale', 'int', 'Display Interval (months)', 1, 1200, 1]]

class Time(object):
	"""Represents a parent class for managing the simulation time."""

class Env(object):
	"""Represents a parent class for controlling the simulation environment."""

class Agt(object):
	"""Represents a parent class for implementing agents."""


class NomadTime(Time):
	"""Controls the time structure and change in the model."""

	def __init__(self, lengthYear=12, lengthGeneration=10, lengthRun=1000):
		"""Constructor of the time agent.

		Args:
			lengthYear: number of months in one year
			lengthGeneration: number of years in one generation
			lengthRun: number of generations in one simulation run
		"""
		self.monthsInYear = lengthYear
		self.yearsInGeneration = lengthGeneration
		self.generationsInRun = lengthRun
		self.month = 0
		self.year = 0
		self.generation = 0

	def Update(self):
		"""Updates each component (month, year, generation) of the time structure.

		Returns:
			False when the last generation has been reached, and the simulation should end.
		"""
		self.month += 1
		if self.monthsInYear == 1 or self.month % self.monthsInYear == 1:
			self.month = 1
			self.year += 1
			if self.yearsInGeneration == 1 or self.year % self.yearsInGeneration == 1:
				self.year = 1
				self.generation += 1
				if self.generation > self.generationsInRun:
					return False
		return True

	def TimeInMonth(self):
		"""Measures the current time in the total number of months passed from the start of simulation.

		Returns:
			Time in month.
		"""
		return self.month + (self.year - 1) * self.monthsInYear + (self.generation - 1) * self.yearsInGeneration * self.monthsInYear


class WestAfrica(Env):
	"""Represents a modeled dryland landscape transplanted from 'West African Nomads Game' in Rice 1975."""

	def __init__(self, name='West Africa', width=18, height=19, val=1.0, dProbs=[23.0/36.0, 4.0/36.0], dSeverity=[0.5, 1.0, 0.5], tProbs=[7.0/36.0, 18.0/36.0, 4.0/36.0], tSeverity=[0.25, 0.5, 1.0], blockTime=3):
		"""Constructor of the environment.

		Args:
			name: name of the environment
			width: width of landscape
			height: height of landscape
			val: maximum value (or scale) of grazing resources
			dProbs: probability distribution of drought events ([moderate drought, major drought])
			dSeverity: effect of each type of drought event ([moderate drought, major drought, major drought on riverine areas])
			tProbs: probability distribution of tsetse events ([minor tsetse, moderate tsetse, massive tsetse])
			tSeverity: effect of each type of tsetse event ([minor tsetse, moderate tsetse, massive tsetse])
			blockTime: number of months for blocking access to grazed sites
		"""
		self.name = name
		self.width = width
		self.height = height
		self.resourceValue = val
		self.droughtProbs = dProbs
		self.droughtSeverity = dSeverity
		self.tsetseProbs = tProbs
		self.tsetseSeverity = tSeverity
		self.resources = SP.zeros([height, width])
		self.factor1 = SP.zeros([height, width])
		self.factor2 = SP.zeros([height, width])
		self.access = SP.ones([height, width])
		self.clock = SP.zeros([height, width])
		self.category1 = SP.zeros([height, width])
		self.category2 = SP.zeros([height, width])
		self.blockTime = blockTime

	def ReadData(self, data_dir=os.path.join('Input', 'WestAfricanNomads'), file1='LCLU.csv', file2='ClimateZones.csv'):
		"""Reads two csv data files, builds a spatial makeup of the landscape.

		Args:
			data_dir: string directory name
			file1: string name of the first file (land cover and land use) to read
			file2: string name of the first file (climate zones) to read
		"""
		if not file1.endswith('.csv'):
			file1 += '.csv'
		if not file2.endswith('.csv'):
			file2 += '.csv'
		filename = os.path.join(data_dir, file1)
		reader = csv.reader(open(filename, 'rU'))
		for i, line in enumerate(reader):
			if len(line) == self.width:
				self.category1[self.height - 1 - i] = [int(data) for data in line]
		filename = os.path.join(data_dir, file2)
		reader = csv.reader(open(filename, 'rU'))
		for i, line in enumerate(reader):
			if len(line) == self.width:
				self.category2[self.height - 1 - i] = [int(data) for data in line]

	def Update(self, time):
		"""Updates the landscape conditions.

		Args:
			time: a NomadTime agent
		"""
		for i in xrange(self.width):
			for j in xrange(self.height):
				if self.category1[j][i] != 3:
					self.factor1[j][i] = self.Drought(i, j, time)
					self.factor2[j][i] = self.Tsetse(i, j, time)
					if self.access[j][i] == 0.0:
						self.clock[j][i] += 1
						if self.clock[j][i] > self.blockTime:
							self.access[j][i] = 1.0
							self.clock[j][i] = 0
					self.resources[j][i] = self.access[j][i] * self.resourceValue * (1.0 - self.factor1[j][i]) * (1.0 - self.factor2[j][i])

	def Drought(self, x, y, time):
		"""Stochastically generates a drought event.

		Args:
			x: x coordinate of the location under consideration
			y: y coordinate of the location under consideration
			time: a NomadicTime agent

		Returns:
			severity of the drought
		"""
		if self.category2[y][x] == 0 and time.month >= 5 and time.month <= 8:
			return 0.0
		elif self.category2[y][x] == 1 and time.month >= 4 and time.month <= 9:
			return 0.0
		elif self.category2[y][x] == 2 and time.month >= 3 and time.month <= 10:
			return 0.0
		elif self.category2[y][x] == 3:
			return 0.0
		lottery = RD.random()
		if lottery <= self.droughtProbs[0]:
			# moderate drought
			if self.category1[y][x] != 1:
				return self.droughtSeverity[0]
		elif lottery <= self.droughtProbs[0] + self.droughtProbs[1]:
			# major drought
			if self.category1[y][x] != 1:
				return self.droughtSeverity[1]
			else:
				return self.droughtSeverity[2]
		return 0.0

	def Tsetse(self, x, y, time):
		"""Stochastically generates a tsetse fly event.

		Args:
			x: x coordinate of the location under consideration
			y: y coordinate of the location under consideration
			time: a NomadicTime agent

		Returns:
			severity of the disease spread
		"""
		if self.category1[y][x] == 2:
			return 0.0
		elif self.category2[y][x] == 0 and (time.month <= 5 or time.month >= 8):
			return 0.0
		elif self.category2[y][x] == 1 and (time.month <= 5 or time.month >= 8):
			return 0.0
		elif self.category2[y][x] == 2 and (time.month <= 4 or time.month >= 9):
			return 0.0
		elif self.category2[y][x] == 3 and (time.month <= 2 or time.month >= 11):
			return 0.0
		lottery = RD.random()
		if lottery <= self.tsetseProbs[0]:
			# minor tsetse
			return self.tsetseSeverity[0]
		elif lottery <= self.tsetseProbs[0] + self.tsetseProbs[1]:
			# moderate tsetse
			return self.tsetseSeverity[1]
		elif lottery <= self.tsetseProbs[0] + self.tsetseProbs[1] + self.tsetseProbs[2]:
			# massive tsetse
			return self.tsetseSeverity[2]
		return 0.0

	def Draw(self, mode='LCLU'):
		"""Displays the landscape condition based on a selected variable.

		Args:
			mode: the landscape variable to be shown (0: LCLU, 1: ClimateZones, 2: Resources, 3: Drought, 4: Tsetse)
		Returns
			the displayed profile
		"""
		if mode == 'LCLU' or mode == 0:
			cond = 'LCLU'
			PL.pcolormesh(self.category1, vmin = -6, vmax = 4, cmap = PL.cm.gist_earth)
		elif mode == 'ClimateZones' or mode == 1:
			cond ='ClimateZones'
			PL.pcolormesh(self.category2, vmin = -1, vmax = 5, cmap = PL.cm.ocean_r)
		elif mode == 'Resources' or mode == 2:
			cond = 'Resources'
			PL.pcolormesh(self.resources, vmin = 0, vmax = 1, cmap = PL.cm.Greens)
		elif mode == 'Drought' or mode == 3:
			cond = 'Drought'
			PL.pcolormesh(self.factor1, vmin = 0, vmax = 1, cmap = PL.cm.autumn_r)
		elif mode == 'Tsetse' or mode == 4:
			cond = 'Tsetse'
			PL.pcolormesh(self.factor2, vmin = 0, vmax = 1, cmap = PL.cm.autumn_r)
		return cond


class Nomad(Agt):
	"""Represents a nomadic pastoral unit moving with livestock over the landscape."""

	def __init__(self, id=0, lengthRoute=12, adaptInterval=10, mRange=5, gRange=0, sRange=5, sFreq=0.1, numAlts=100, exploration=0.1, env=None):
		"""Constructor of the agent.

		Args:
			id: a unique ID of the agent
			lengthRoute: length of the agent's movement plan
			adaptInterval: number of movement cycles before the route update occurs
			mRange: distance that can be moved without incurring costs
			gRange: range of neighborhood to be grazed
			sRange: range of neighborhood to be scouted for information
			sFreq: scouting frequency in one month
			numAlts: maximum number of alternative routes to be considered in the adaptation phase
			exploration: tendency to allow major changes in the movement plan
			env: a landscape agent (Env object) to be linked with
		"""
		self.id = id
		self.lengthOfRoute = lengthRoute
		self.adaptationInterval = adaptInterval
		self.moveRange = mRange
		self.grazeRange = gRange
		self.scoutRange = sRange
		self.scoutFrequency = sFreq
		self.numOfAlts = numAlts
		self.exploreTendency = exploration
		self.env = env
		xs = [RD.randint(0, env.width - 1) for i in xrange(self.lengthOfRoute)]
		ys = [RD.randint(0, env.height - 1) for i in xrange(self.lengthOfRoute)]
		self.route = SP.array([xs, ys])
		self.x = self.route[0][0]
		self.y = self.route[1][0]
		self.clock = 0
		self.cycle = 0
		self.generation = 0
		self.knowledge = [[] for i in xrange(self.lengthOfRoute)]
		self.resources = SP.zeros(self.lengthOfRoute)
		self.costs = SP.zeros(self.lengthOfRoute)
		self.potential = 0.0
		if id == 0:
			self.color = (1.0, 0.0, 0.0)
		else:
			self.color = (RD.random(), RD.random(), RD.random())

	def Update(self, moveCost=10, adaptParams=[-12.0, 100, 0.00001, 0.01], env=None, rec=None):
		"""Iterates the agent's update rule.

		Args:
			moveCost: cost incurred when moving faster than at a specified speed
			adaptParams: : parameters controlling the agent's update of the movement plan ([minimum potential normalized to 0.0, reference potential normalized to 1.0, adaptation noise for minimum potential, daptation noise for reference potential])
			env: a landscape agent (Env object) to be linked with
			rec: a recording agent (Recorder object)
		"""
		self.clock += 1
		if self.lengthOfRoute == 1 or self.clock % self.lengthOfRoute == 1:
			self.clock = 1
			self.cycle += 1
			if self.adaptationInterval == 1 or self.cycle % self.adaptationInterval == 1:
				self.cycle = 1
				self.generation += 1
		self.Move(moveCost=moveCost, env=env)
		self.Scout(env=env)
		if self.clock == self.lengthOfRoute and self.cycle == self.adaptationInterval:
			nextRoute, data = self.Adapt(moveCost=moveCost, adaptParams=adaptParams)
			self.Report(rec=rec)
			self.Renew(nextRoute, data, inheritance)

	def Move(self, moveCost, env):
		"""Moves on the landscape for grazing resources for the livestock.

		Args:
			moveCost: cost incurred when moving faster than at a specified speed
			env: a landscape agent (Env object) to be linked with
		"""
		self.x = self.route[0][self.clock - 1]
		self.y = self.route[1][self.clock - 1]
		res = 0
		for i in xrange(-self.grazeRange, self.grazeRange + 1):
			curX = self.x + i
			if curX >= 0 and curX < env.width:
				for j in xrange(-self.grazeRange, self.grazeRange + 1):
					curY = self.y + j
					if curY >= 0 and curY < env.height and ((curX - self.x) ** 2 + (curY - self.y) ** 2 <= self.grazeRange ** 2):
						res += env.resources[curY][curX]
						env.access[curY][curX] = 0.0
		self.resources[self.clock - 1] = (self.resources[self.clock - 1] * (self.cycle - 1) + res) / self.cycle
		if self.cycle == 1:
			if self.clock == 1:
				dist = (self.route[0][self.clock - 1] - self.route[0][self.lengthOfRoute - 1]) ** 2 + (self.route[1][self.clock - 1] - self.route[1][self.lengthOfRoute - 1]) ** 2
			else:
				dist = (self.route[0][self.clock - 1] - self.route[0][self.clock - 2]) ** 2 + (self.route[1][self.clock - 1] - self.route[1][self.clock - 2]) ** 2
			if dist > self.moveRange ** 2:
				self.costs[self.clock - 1] = moveCost

	def Scout(self, env):
		"""Stochastically scouts a nearby site for information on grazing resources.

		Args:
			env: a landscape agent (Env object) to be linked with
		"""
		if RD.random() < self.scoutFrequency:
			while 1:
				dX = RD.choice(range(-self.scoutRange, self.scoutRange + 1))
				dY = RD.choice(range(-self.scoutRange, self.scoutRange + 1))
				if dX == 0 and dY == 0:
					continue
				if dX ** 2 + dY ** 2 > self.scoutRange ** 2:
					continue
				tgtX = self.x + dX
				tgtY = self.y + dY
				if (tgtX >= 0 and tgtX < env.width) and (tgtY >= 0 and tgtY < env.height):
					break
			res = 0
			for i in xrange(-self.grazeRange, self.grazeRange + 1):
				curX = tgtX + i
				if curX >= 0 and curX < env.width:
					for j in xrange(-self.grazeRange, self.grazeRange + 1):
						curY = tgtY + j
						if curY >= 0 and curY < env.height and ((curX - tgtX) ** 2 + (curY - tgtY) ** 2 <= self.grazeRange ** 2):
							res += env.resources[curY][curX]
			search = False
			for info in self.knowledge[self.clock - 1]:
				if info[0] == tgtX and info[1] == tgtY:
					search = True
					info[2] += 1
					info[3] = (info[3] * (info[2] - 1) + res) / info[2]
					break
			if not search:
				info = [tgtX, tgtY, 1, res]
				self.knowledge[self.clock - 1].append(info)

	def Adapt(self, moveCost, adaptParams):
		"""Generates the next movement plan in an adaptive manner.

		Args:
			moveCost: cost incurred when moving faster than at a specified speed
			adaptParams: : parameters controlling the agent's update of the movement plan ([minimum potential normalized to 0.0, reference potential normalized to 1.0, adaptation noise for minimum potential, daptation noise for reference potential])
		Returns:
			nextRoute: the chosen movement plan for the next generation
			data: integretaed knowledge obtained in the current generation
		"""

		# integrate scouting knowledge with information obtained through the yearly movement
		data = [[] for i in xrange(self.lengthOfRoute)]
		num = 1
		for i in xrange(self.lengthOfRoute):
			data[i].append([self.route[0][i], self.route[1][i], self.adaptationInterval, self.resources[i]])
			for info in self.knowledge[i]:
				if info[0] == self.route[0][i] and info[1] == self.route[1][i]:
					data[i][0][2] += info[2]
					data[i][0][3] = (data[i][0][2] * data[i][0][3] + info[2] * info[3]) / (data[i][0][2] + info[2])
				else:
					data[i].append(info)
			if num < self.numOfAlts:
				num *= len(data[i])
				if num > self.numOfAlts:
					num = self.numOfAlts

		# stochastically generate and evaluate alternative movement plans
		routes = [[0 for i in xrange(self.lengthOfRoute)]]
		potentials = SP.zeros(num)
		self.potential = (-1 * sum(self.resources) + sum(self.costs) - adaptParams[0]) / (adaptParams[1] - adaptParams[0])
		potentials[0] = self.potential
		for i in xrange(num - 1):
			while 1:
				alt = []
				for j in xrange(self.lengthOfRoute):
					if len(data[j]) > 1 and RD.random() < self.exploreTendency:
						alt.append(RD.choice(range(1, len(data[j]))))
					else:
						alt.append(0)
				if routes.count(alt) == 0:
					routes.append(alt)
					break
			potential = 0
			for j in xrange(self.lengthOfRoute):
				info = data[j][alt[j]]
				potential -= info[3]
				if j == 0:
					dist = (info[0] - data[self.lengthOfRoute - 1][alt[self.lengthOfRoute - 1]][0]) ** 2 + (info[1] - data[self.lengthOfRoute - 1][alt[self.lengthOfRoute - 1]][1]) ** 2
				else:
					dist = (info[0] - data[j - 1][alt[j - 1]][0]) ** 2 + (info[1] - data[j - 1][alt[j - 1]][1]) ** 2
				if dist > self.moveRange ** 2:
					potential += moveCost
			potential = (potential - adaptParams[0]) / (adaptParams[1] - adaptParams[0])
			potentials[i + 1] = potential
		temperature = (adaptParams[3] - adaptParams[2]) * self.potential + adaptParams[2]
		probs = SP.exp(-(1 / temperature) * potentials)

		# stochastically select the next movement plan among the alternatives
		lottery = sum(probs) * RD.random()
		for k, prob in enumerate(probs):
			lottery -= prob
			if lottery <= 0:
				break
		alt = routes[k]
		xs = [0 for i in xrange(self.lengthOfRoute)]
		ys = [0 for i in xrange(self.lengthOfRoute)]
		nextRoute = SP.array([xs, ys])
		for i in xrange(self.lengthOfRoute):
			nextRoute[0][i] = data[i][alt[i]][0]
			nextRoute[1][i] = data[i][alt[i]][1]

		return nextRoute, data

	def Renew(self, nextRoute=None, data=None, inheritance=False):
		"""Renew the state of the nomad by initializng some of its variables.

		Args:
			nextRoute: a new movement plan
			data: knowledge to be inherited
			inheritance: an option for activating knowledge inheritance between generations
		"""
		if nextRoute == None:
			xs = [RD.randint(0, env.width - 1) for i in xrange(self.lengthOfRoute)]
			ys = [RD.randint(0, env.height - 1) for i in xrange(self.lengthOfRoute)]
			self.route = SP.array([xs, ys])
		else:
			self.route = nextRoute
		self.x = self.route[0][0]
		self.y = self.route[1][0]
		if data == None or inheritance == False:
			self.knowledge = [[] for i in xrange(self.lengthOfRoute)]
		else:
			self.knowledge = data
		self.resources = SP.zeros(self.lengthOfRoute)
		self.costs = SP.zeros(self.lengthOfRoute)
		self.potential = 0

	def Report(self, rec):
		"""Reports the agent's state (routes, resources, costs, potential) to the Recorder agent.

		Args:
			rec: a recording agent (Recorder object)
		"""
		rec.routes[self.id] = self.route
		rec.resources[self.id] = self.resources
		rec.costs[self.id] = self.costs
		rec.potentials[self.id] = self.potential
		rec.colors[self.id] = self.color

	def Show(self, route=True, size=10):
		"""Displays the agent on 2D landscape map.

		Args:
			route: an option for showing the site locations described by the movement plan
			size: size of the agent icon
		"""
		if route:
			xs = [x + 0.5 for x in self.route[0]]
			xs.append(self.route[0][0] + 0.5)
			ys = [y + 0.5 for y in self.route[1]]
			ys.append(self.route[1][0] + 0.5)
			PL.plot(xs, ys, color=self.color, ls=':', marker='x', alpha=0.8)
			for i in xrange(self.lengthOfRoute):
				curX, curY = self.route[0][i], self.route[1][i]
				PL.annotate(s=str(i + 1), xy=(curX + 0.5, curY + 0.5), color=self.color, ha='center', va='center', alpha=0.8)
				#if i < self.lengthOfRoute - 1:
				#	dX = self.route[0][i + 1] - curX
				#	dY = self.route[1][i + 1] - curY
				#else:
				#	dX = self.route[0][0] - curX
				#	dY = self.route[1][0] - curY
				#PL.arrow(curX + 0.5, curY + 0.5, dX, dY, length_includes_head=True, head_width=0.4, ls='dotted', color=self.color, alpha=1)
		PL.scatter([self.x + 0.5], [self.y + 0.5], s=size ** 2, color=self.color)


class Recorder(Agt):
	"""Collects and arranges various data on the simulation process, converting them to appropriate file formats."""

	def __init__(self, data_dir='Output', pop=1, lengthRoute=12, setting=True, process=True, snap=True, shots=False, sInterval=100):
		"""Constructor of the agent.

		Args:
			data_dir: string directory name
			pop: population of Nomad agents
			lengthRoute: length of an agent's movement plan
			setting: if True, write out information on simulation setting
			files: if True, write out recorded data on the simulation process in a csv format
			snap: if True, record and capture a snapshot of the situation where the average yearly resources obtained by agents is highest.
			shots: taking successive snapshots of a simulation run at a specified interval of generations
		"""
		self.data_dir = data_dir
		self.population = pop
		self.lengthOfRoute = lengthRoute
		self.settingOut = setting
		self.processOut = process
		self.snapshot = snap
		self.succesiveShots = shots
		self.shotInterval = sInterval
		self.routes = SP.zeros([self.population, 2, self.lengthOfRoute], dtype=SP.int16)
		self.resources = SP.zeros([self.population, self.lengthOfRoute])
		self.costs = SP.zeros([self.population, self.lengthOfRoute])
		self.potentials = SP.zeros(self.population)
		self.colors = SP.zeros([self.population, 3])
		self.grossPotential = 0.0
		self.resourceTimeMeans = SP.zeros(self.population)
		self.resourceTimeStds = SP.zeros(self.population)
		self.resourceTimeAgtMean = 0.0
		self.resourceTimeMeanMax = 0.0
		self.resourceTimeMeanMedian = 0.0
		self.resourceTimeMeanMin = 0.0
		self.resourceTimeMeanStd = 0.0
		self.resourceTimeStdAgtMean = 0.0
		self.maxNomadID = -1
		self.medianNomadID = -1
		self.minNomadID = -1
		self.costTimeMeans = SP.zeros(self.population)
		self.costTimeAgtMean = 0.0
		self.displacementVectors = SP.zeros([self.population, 2, self.lengthOfRoute])
		self.displacementAgtMeans = SP.zeros([2, self.lengthOfRoute])
		self.distances = SP.zeros([self.population, self.lengthOfRoute])
		self.distanceTimeMeans = SP.zeros(self.population)
		self.distanceAgtMeans = SP.zeros(self.lengthOfRoute)
		self.distanceTimeAgtMean = 0.0
		self.distanceTimeMeanStd = 0.0
		self.distanceMax = 0.0
		self.distanceMin = 0.0
		self.synchros = SP.zeros(self.lengthOfRoute)
		self.synchroTimeMean = 0.0
		self.centroids = SP.zeros([self.population, 2])
		self.centroidAgtMean = SP.array([0.0, 0.0])
		self.momentXs = SP.zeros(self.population)
		self.momentXAgtMean = 0.0
		self.momentXMax = 0.0
		self.momentXMin = 0.0
		self.momentXStd = 0.0
		self.momentYs = SP.zeros(self.population)
		self.momentYAgtMean = 0.0
		self.momentYMax = 0.0
		self.momentYMin = 0.0
		self.momentYStd = 0.0
		self.maxRanges = SP.zeros(self.population)
		self.maxRangeMean = 0.0
		self.maxRangeMax = 0.0
		self.maxRangeMin = 0.0
		self.maxRangeStd = 0.0
		self.maxNomadResources = 0.0
		self.maxNomadDistance = 0.0
		self.maxNomadCentroid = SP.array([0.0, 0.0])
		self.maxNomadMomentX = 0.0
		self.maxNomadMomentY = 0.0
		self.maxNomadMaxRange = 0.0
		self.medianNomadResources = 0.0
		self.medianNomadDistance = 0.0
		self.medianNomadCentroid = SP.array([0.0, 0.0])
		self.medianNomadMomentX = 0.0
		self.medianNomadMomentY = 0.0
		self.medianNomadMaxRange = 0.0
		self.minNomadResources = 0.0
		self.minNomadDistance = 0.0
		self.minNomadCentroid = SP.array([0.0, 0.0])
		self.minNomadMomentX = 0.0
		self.minNomadMomentY = 0.0
		self.minNomadMaxRange = 0.0
		self.recordedGeneration = -1
		self.recordedMeanResources = 0.0
		self.recordedRoutes = SP.zeros([self.population, 2, self.lengthOfRoute], dtype=SP.int16)
		self.recordedResources = SP.zeros(self.population)
		self.recordedCosts = SP.zeros(self.population)
		self.recordedVectors = SP.zeros([self.population, 2, self.lengthOfRoute])
		self.recordedMeanDistance = SP.zeros(self.population)
		self.recordedCentroids = SP.zeros([self.population, 2])
		self.recordedMomentXs = SP.zeros(self.population)
		self.recordedMomentYs = SP.zeros(self.population)
		self.recordedMaxRanges = SP.zeros(self.population)

	def Update(self, run=1, time=None, env=None, mode='LCLU'):
		"""Arranges the simulation data and writes them out at the end of each generation.

		Args:
			run: number of the present simulation run
			time: a NomadTime agent
			env: a landscape agent (an Env object)
			mode: the landscape variable to be shown (0: LCLU, 1: ClimateZones, 2: Resources, 3: Drought, 4: Tsetse)
		"""
		if time.month < time.monthsInYear or time.year < time.yearsInGeneration:
			return

		# recording the setting specification
		if settingOut and run == 1 and time.generation == 1:
			filename = os.path.join(self.data_dir, 'setting.csv')
			fp = open(filename, 'a')
			dt = datetime.now()
			fp.write(dt.strftime("%Y%m%d%H%M") + '\n')
			line = 'MonthsInYear,YearsInGeneration,GenerationsInRun,Repetition,Input1,Input2,ResourceValue,DroughtProbs,DroughtSeverity,TsetseProbs,TsetseSeverity,Population,MoveRange,GrazeRange,ScoutRange,ScoutFrequency,NumOfAlts,ExploreTendency,MovementCost,AdaptParams,Inheritance\n'
			fp.write(line)
			line = str(time.monthsInYear) + ',' + str(time.yearsInGeneration) + ',' + str(time.generationsInRun) + ',' + str(repetition) + ',' + str(input1) + ',' + str(input2) + ',' + str(env.resourceValue) + ',' + '[' + str(env.droughtProbs[0]) + ' ' + str(env.droughtProbs[1]) + ']' + ',' + '[' + str(env.droughtSeverity[0]) + ' ' + str(env.droughtSeverity[1]) + ' ' + str(env.droughtSeverity[2]) + ']' + ',' + '[' + str(env.tsetseProbs[0]) + ' ' + str(env.tsetseProbs[1]) + ' ' + str(env.tsetseProbs[2]) + ']' + ',' + '[' + str(env.tsetseSeverity[0]) + ' ' + str(env.tsetseSeverity[1]) + ' ' + str(env.tsetseSeverity[2]) + ']' + ',' + str(population) + ',' + str(moveRange) + ',' + str(grazeRange) + ',' + str(scoutRange) + ',' + str(scoutFrequency) + ',' + str(numOfAlts) + ',' + str(exploreTendency) + ',' + str(movementCost) + ',' + '[' + str(adaptParams[0]) + ' ' + str(adaptParams[1]) + ' ' + str(adaptParams[2]) + ' ' + str(adaptParams[3]) + ']' + ',' + str(inheritance) + '\n'
			fp.write(line)
			fp.close()

		# calculating and writing out the output variables
		if processOut:
			self.grossPotential = sum(self.potentials)
			self.resourceTimeMeans = SP.mean(self.resources, 1)
			self.resourceTimeStds = SP.std(self.resources, 1)
			self.resourceTimeAgtMean = SP.mean(self.resourceTimeMeans)
			self.resourceTimeMeanMax = SP.amax(self.resourceTimeMeans)
			if self.population % 2 == 1:
				self.resourceTimeMeanMedian = SP.median(self.resourceTimeMeans)
			else:
				self.resourceTimeMeanMedian = SP.sort(self.resourceTimeMeans)[self.population / 2 - 1]
			self.resourceTimeMeanMin = SP.amin(self.resourceTimeMeans)
			self.resourceTimeMeanStd = SP.std(self.resourceTimeMeans)
			self.resourceTimeStdAgtMean = SP.mean(self.resourceTimeStds)
			self.maxNomadID = SP.argmax(self.resourceTimeMeans)
			for k, x in enumerate(self.resourceTimeMeans):
				if x == self.resourceTimeMeanMedian:
					break
			self.medianNomadID = k
			self.minNomadID = SP.argmin(self.resourceTimeMeans)
			self.costTimeMeans = SP.mean(self.costs, 1)
			self.costTimeAgtMean = SP.mean(self.costTimeMeans)
			for i in xrange(self.population):
				xs = self.routes[i][0]
				ys = self.routes[i][1]
				self.maxRanges[i] = 0
				for j in xrange(self.lengthOfRoute):
					if j == self.lengthOfRoute - 1:
						self.displacementVectors[i][0][j] = xs[0] - xs[j]
						self.displacementVectors[i][1][j] = ys[0] - ys[j]
					else:
						self.displacementVectors[i][0][j] = xs[j + 1] - xs[j]
						self.displacementVectors[i][1][j] = ys[j + 1] - ys[j]
						for k in xrange(j + 1, self.lengthOfRoute):
							if (xs[k] - xs[j]) ** 2 + (ys[k] - ys[j]) ** 2 > self.maxRanges[i] ** 2:
								self.maxRanges[i] = math.sqrt((xs[k] - xs[j]) ** 2 + (ys[k] - ys[j]) ** 2)
				self.distances[i] = SP.sqrt(self.displacementVectors[i][0] ** 2 + self.displacementVectors[i][1] ** 2)
				self.centroids[i][0] = SP.mean(xs)
				self.centroids[i][1] = SP.mean(ys)
				self.momentXs[i] = SP.std(xs)
				self.momentYs[i] = SP.std(ys)
			self.displacementAgtMeans = SP.mean(self.displacementVectors, 0)
			self.distanceTimeMeans = SP.mean(self.distances, 1)
			self.distanceAgtMeans = SP.mean(self.distances, 0)
			self.distanceTimeAgtMean = SP.mean(self.distanceTimeMeans)
			self.distanceTimeMeanStd = SP.std(self.distanceTimeMeans)
			self.distanceMax = SP.amax(self.distances)
			self.distanceMin = SP.amin(self.distances)
			self.synchros = SP.sqrt((self.population * self.displacementAgtMeans[0]) ** 2 + (self.population * self.displacementAgtMeans[1]) ** 2) / (self.population * self.distanceAgtMeans)
			self.synchroTimeMean = SP.mean(self.synchros)
			self.centroidAgtMean = SP.mean(self.centroids, 0)
			self.momentXAgtMean = SP.mean(self.momentXs)
			self.momentXMax = SP.amax(self.momentXs)
			self.momentXMin = SP.amin(self.momentXs)
			self.momentXStd = SP.std(self.momentXs)
			self.momentYAgtMean = SP.mean(self.momentYs)
			self.momentYMax = SP.amax(self.momentYs)
			self.momentYMin = SP.amin(self.momentYs)
			self.momentYStd = SP.std(self.momentYs)
			self.maxRangeMean = SP.mean(self.maxRanges)
			self.maxRangeMax = SP.amax(self.maxRanges)
			self.maxRangeMin = SP.amin(self.maxRanges)
			self.maxRangeStd = SP.std(self.maxRanges)
			self.maxNomadResources = self.resourceTimeMeans[self.maxNomadID]
			self.maxNomadDistance = self.distanceTimeMeans[self.maxNomadID]
			self.maxNomadCentroid = self.centroids[self.maxNomadID]
			self.maxNomadMomentX = self.momentXs[self.maxNomadID]
			self.maxNomadMomentY = self.momentYs[self.maxNomadID]
			self.maxNomadMaxRange = self.maxRanges[self.maxNomadID]
			self.medianNomadResources = self.resourceTimeMeans[self.medianNomadID]
			self.medianNomadDistance = self.distanceTimeMeans[self.medianNomadID]
			self.medianNomadCentroid = self.centroids[self.medianNomadID]
			self.medianNomadMomentX = self.momentXs[self.medianNomadID]
			self.medianNomadMomentY = self.momentYs[self.medianNomadID]
			self.medianNomadMaxRange = self.maxRanges[self.medianNomadID]
			self.minNomadResources = self.resourceTimeMeans[self.minNomadID]
			self.minNomadDistance = self.distanceTimeMeans[self.minNomadID]
			self.minNomadCentroid = self.centroids[self.minNomadID]
			self.minNomadMomentX = self.momentXs[self.minNomadID]
			self.minNomadMomentY = self.momentYs[self.minNomadID]
			self.minNomadMaxRange = self.maxRanges[self.minNomadID]
			filename = os.path.join(self.data_dir, 'dynamics.csv')
			fp = open(filename, 'a')
			if time.generation == 1:
				dt = datetime.now()
				line = 'Run' + str(run) + '(' + dt.strftime("%Y%m%d%H%M") + ')\n'
				fp.write(line)
				line = 'Generation,Year,Month,GrossPotential,ResourceTimeAgtMean,ResourceTimeMeanMax,ResourceTimeMeanMedian,ResourceTimeMeanMin,ResourceTimeMeanStd,ResourceTimeStdAgtMean,CostTimeAgtMean,DisplacementAgtMeansX,DisplacementAgtMeansY,DistanceAgtMeans,DistanceTimeAgtMean,DistanceTimeMeanStd,DistanceMax,DistanceMin,Synchros,SynchroTimeMean,CentroidAgtMean,MomentXAgtMean,MomentXMax,MomentXMin,MomentXStd,MomentYAgtMean,MomentYMax,MomentYMin,MomentYStd,MaxRangeMean,MaxRangeMax,MaxRangeMin,MaxRangeStd,MaxNomadID,MaxNomadResources,MaxNomadDistance,MaxNomadCentroid,MaxNomadMomentX,MaxNomadMomentY,MaxNomadMaxRange,MedianNomadID,MedianNomadResources,MedianNomadDistance,MedianNomadCentroid,MedianNomadMomentX,MedianNomadMomentY,MedianNomadMaxRange,MinNomadID,MinNomadResources,MinNomadDistance,MinNomadCentroid,MinNomadMomentX,MinNomadMomentY,MinNomadMaxRange\n'
				fp.write(line)
			line = str(time.generation) + ',' + str(time.year) + ',' + str(time.month) + ',' + str(self.grossPotential) + ',' + str(self.resourceTimeAgtMean) + ',' + str(self.resourceTimeMeanMax) + ',' + str(self.resourceTimeMeanMedian) + ',' + str(self.resourceTimeMeanMin) + ',' + str(self.resourceTimeMeanStd) + ',' + str(self.resourceTimeStdAgtMean) + ',' + str(self.costTimeAgtMean) + ',' + str(self.displacementAgtMeans[0]).replace(',', '').replace('[', '').replace(']', '').replace('\n', '').strip() + ',' + str(self.displacementAgtMeans[1]).replace(',', '').replace('[', '').replace(']', '').replace('\n', '').strip() + ',' + str(self.distanceAgtMeans).replace(',', '').replace('[', '').replace(']', '').replace('\n', '').strip() + ',' + str(self.distanceTimeAgtMean) + ',' + str(self.distanceTimeMeanStd) + ',' + str(self.distanceMax) + ',' + str(self.distanceMin) + ',' + str(self.synchros).replace(',', '').replace('[', '').replace(']', '').replace('\n', '').strip() + ',' + str(self.synchroTimeMean) + ',' + str(self.centroidAgtMean).replace(',', '').replace('[', '').replace(']', '').replace('\n', '').strip() + ',' + str(self.momentXAgtMean) + ',' + str(self.momentXMax) + ',' + str(self.momentXMin) + ',' + str(self.momentXStd) + ',' + str(self.momentYAgtMean) + ',' + str(self.momentYMax) + ',' + str(self.momentYMin) + ',' + str(self.momentYStd) + ',' + str(self.maxRangeMean) + ',' + str(self.maxRangeMax) + ',' + str(self.maxRangeMin) + ',' + str(self.maxRangeStd) + ',' + str(self.maxNomadID) + ',' + str(self.maxNomadResources) + ',' + str(self.maxNomadDistance) + ',' + str(self.maxNomadCentroid).replace(',', '').replace('[', '').replace(']', '').replace('\n', '').strip() + ',' + str(self.maxNomadMomentX) + ',' + str(self.maxNomadMomentY) + ',' + str(self.maxNomadMaxRange) + ',' + str(self.medianNomadID) + ',' + str(self.medianNomadResources) + ',' + str(self.medianNomadDistance) + ',' + str(self.medianNomadCentroid).replace(',', '').replace('[', '').replace(']', '').replace('\n', '').strip() + ',' + str(self.medianNomadMomentX) + ',' + str(self.medianNomadMomentY) + ',' + str(self.medianNomadMaxRange) + ',' + str(self.minNomadID) + ',' + str(self.minNomadResources) + ',' + str(self.minNomadDistance) + ',' + str(self.minNomadCentroid).replace(',', '').replace('[', '').replace(']', '').replace('\n', '').strip() + ',' + str(self.minNomadMomentX) + ',' + str(self.minNomadMomentY) + ',' + str(self.minNomadMaxRange) + '\n'
			fp.write(line)
			fp.close()
			# possibly adding a procedure for writing out a record on each agent at each generation

		# recording the state of agents when the average yearly resources obtained hit the record
		if self.snapshot:
			if SP.mean(self.resources) >= self.recordedMeanResources:
				self.recordedGeneration = time.generation
				self.recordedMeanResources = SP.mean(self.resources)
				self.recordedRoutes = self.routes
				self.recordedResources = SP.mean(self.resources, 1)
				self.recordedCosts = SP.mean(self.costs, 1)
				if self.processOut:
					self.recordedVectors = self.displacementVectors
					self.recordedMeanDistance = self.distanceTimeMeans
					self.recordedCentroids = self.centroids
					self.recordedMomentXs = self.momentXs
					self.recordedMomentYs = self.momentYs
					self.recordedMaxRanges = self.maxRanges
			if time.generation == time.generationsInRun:
				dt = datetime.now()
				filename = os.path.join(self.data_dir, 'snapshotRecs.csv')
				fp = open(filename, 'a')
				if run == 1:
					line = 'Time,Run,Generation,MeanResources,'
					for i in xrange(self.population):
						nom = 'Nomad' + str(i)
						line = line + nom + 'Route,' + nom + 'Resources,' + nom + 'Cost,' + nom + 'Vecotrs,' + nom + 'Distance,' + nom + 'Centroid,' + nom + 'MomentX,' + nom + 'MomentY,' + nom + 'MaxRange,'
					line.strip(',')
					fp.write(line + '\n')
				line = dt.strftime("%Y%m%d%H%M") + ',' + str(run) + ',' + str(self.recordedGeneration) + ',' + str(self.recordedMeanResources) + ','
				for i in xrange(self.population):
					line = line + str(zip(self.recordedRoutes[i][0], self.recordedRoutes[i][1])).replace(',', '').replace('[', '').replace(']', '').replace('\n', '').strip() + ',' + str(self.recordedResources[i]) + ',' + str(self.recordedCosts[i]) + ',' + str(zip(self.recordedVectors[i][0], self.recordedVectors[i][1])).replace(',', '').replace('[', '').replace(']', '').replace('\n', '').strip() + ',' + str(self.recordedMeanDistance[i]) + ',' + str(self.recordedCentroids[i]).replace(',', '').replace('[', '').replace(']', '').replace('\n', '').strip() + ',' + str(self.recordedMomentXs[i]) + ',' + str(self.recordedMomentYs[i]) + ',' + str(self.recordedMaxRanges[i]) + ','
				line.strip(',')
				fp.write(line + '\n')
				fp.close()
				PL.cla()
				cond = env.Draw(mode)
				PL.hold(True)
				for i in xrange(self.population):
					xs = [x + 0.5 for x in self.routes[i][0]]
					xs.append(self.routes[i][0][0] + 0.5)
					ys = [y + 0.5 for y in self.routes[i][1]]
					ys.append(self.routes[i][1][0] + 0.5)
					PL.plot(xs, ys, color=self.colors[i], ls=':', marker='o', ms=15, mec=self.colors[i], mfc=self.colors[i], alpha=1.0)
					for j in xrange(self.lengthOfRoute):
						curX, curY = self.routes[i][0][j], self.routes[i][1][j]
						PL.annotate(s=str(j + 1), xy=(curX + 0.5, curY + 0.5), color='w', size='x-small', ha='center', va='center', alpha=1.0)
				PL.hold(False)
				PL.axis('off')
				PL.xlim([0, env.width - 1])
				PL.ylim([0, env.height - 1])
				PL.axis('image')
				PL.tick_params(axis='both', length=0)
				#PL.title('{name} ({cond}): run={run} gen={gen} year={year} month={month}'.format(name=env.name, cond=cond, run=run, gen=self.recordedGeneration, year=time.year, month=time.month))
				filename = os.path.join(self.data_dir, '{name}({cond})_rec_run{run}_gen{gen}_{time}.png'.format(name=env.name, cond=cond, run=run, gen=self.recordedGeneration, time=dt.strftime("%Y%m%d%H%M")))
				PL.savefig(filename, format='png', frameon=False)

		# taking successive snapshots of a simulation run at a specified interval
		if self.succesiveShots:
			if time.generation % self.shotInterval == 0:
				dt = datetime.now()
				PL.cla()
				cond = env.Draw(mode)
				PL.hold(True)
				for i in xrange(self.population):
					xs = [x + 0.5 for x in self.routes[i][0]]
					xs.append(self.routes[i][0][0] + 0.5)
					ys = [y + 0.5 for y in self.routes[i][1]]
					ys.append(self.routes[i][1][0] + 0.5)
					PL.plot(xs, ys, color=self.colors[i], ls=':', marker='o', ms=15, mec=self.colors[i], mfc=self.colors[i], alpha=1.0)
					for j in xrange(self.lengthOfRoute):
						curX, curY = self.routes[i][0][j], self.routes[i][1][j]
						PL.annotate(s=str(j + 1), xy=(curX + 0.5, curY + 0.5), color='w', size='x-small', ha='center', va='center', alpha=1.0)
				PL.hold(False)
				PL.axis('off')
				PL.xlim([0, env.width - 1])
				PL.ylim([0, env.height - 1])
				PL.axis('image')
				PL.tick_params(axis='both', length=0)
				filename = os.path.join(self.data_dir, '{name}({cond})_run{run}_gen{gen}_{time}.png'.format(name=env.name, cond=cond, run=run, gen=time.generation, time=dt.strftime("%Y%m%d%H%M")))
				PL.savefig(filename, format='png', frameon=False)

		print 'generation:', time.generation
		print 'gross potential  mean resources  mean cost'
		print sum(self.potentials), SP.mean(self.resources), SP.mean(self.costs)


def init():
	global run, time, env, noms, rec
	global repetition, generationsInRun, population, outInterval
	generationsInRun, population, outInterval = tuple(conVars)

	run = True
	time = NomadTime(monthsInYear, yearsInGeneration, generationsInRun)
	env = WestAfrica(dSeverity=droughtSeverity, tSeverity=tsetseSeverity)
	env.ReadData(file1=input1, file2=input2)
	noms = [Nomad(id=i, lengthRoute=monthsInYear, adaptInterval=yearsInGeneration, mRange=moveRange, gRange=grazeRange, sRange=scoutRange, sFreq=scoutFrequency, numAlts=numOfAlts, exploration=exploreTendency, env=env) for i in xrange(population)]
	rec = Recorder(pop=population, lengthRoute=monthsInYear, setting=settingOut, process=processOut, snap=snapshot, shots=succesiveShots, sInterval=shotInterval)


def draw():
	global rpt, run, time, env, noms
	global generationsInRun, population, outInterval
	#dummy1, dummy2, dummy3 = tuple(conVars)

	if run and (outInterval == 1 or time.month == 0 or time.TimeInMonth() % outInterval == 0):
		PL.cla()
		cond = env.Draw(drawingMode)
		PL.hold(True)
		for nom in noms:
			nom.Show(route=showRoute)
		PL.hold(False)
		PL.axis('off')
		PL.xlim([0, env.width - 1])
		PL.ylim([0, env.height - 1])
		PL.axis('image')
		PL.tick_params(axis='both', length=0)
		PL.title('{name} ({cond}): run={run} gen={gen} year={year} month={month}'.format(name=env.name, cond=cond, run=repetition+1-rpt, gen=time.generation, year=time.year, month=time.month))


def step():
	global gui, rpt, run, time, env, noms, rec
	global generationsInRun, population, outInterval
	#dummy1, dummy2, dummy3 = tuple(conVars)

	if run:
		run = time.Update()
		if not run:
			rpt -= 1
			if rpt <= 0:
				gui.runEvent()
			else:
				gui.resetModel()
				gui.runEvent()
		env.Update(time)
		agts = RD.sample(noms, len(noms))
		for agt in agts:
			agt.Update(moveCost=movementCost, adaptParams=adaptParams, env=env, rec=rec)
		rec.Update(run=repetition+1-rpt, time=time, env=env, mode=drawingMode)


def main(name, *args):
	global gui, rpt

	rpt = repetition
	gui = pymas.GUI(title='Nomads Model', conVars=conVars, conSetting=conSetting)
	gui.start(func=[init,draw,step])

if __name__ == '__main__':
	main(*sys.argv)
